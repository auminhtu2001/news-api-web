<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<style>
.container{
  margin-top: 20px;
  width: 75%;
}

.title{
  margin-top: 20px;
  margin-bottom: 20px;
}

.single-news{
  background-color: #ddd;
  padding: 30px;
  margin-bottom: 20px;
  margin-top: 20px;
}
</style>

<title> NEWS </title>

<div class="container">
  <h1 class="title text-center">Free News API</h1>
  <hr>
  <div class="list-wrapper">
    <?php
        $api_url = 'https://newsapi.org/v2/everything?q=apple&from=2021-06-05&to=2021-06-05&sortBy=popularity&apiKey=c4e16cb8e02a4030abb2c6d2cd7adb34';
        $newslis = file_get_contents($api_url);
        $newslis = json_decode($newslis);
      // }
      foreach($newslis -> articles as $news){
    ?>
      <div class="row single-news">
        <div class="col-4">
          <img style="width:100%;" src="<?php echo $news -> urlToImage;?>">
        </div>
        <div class="col-8">
          <h2><?php echo $news->title;?></h2>
          <small><?php echo $news -> source -> name;?></small>
          <?php if($news -> author && $news -> author!=''){ ?>
            <small>| <?php echo $news -> author;?></small>
          <?php } ?>
          <p><?php echo $news -> description;?></p>
          <a href="<?php echo $news->url;?>" class="btn btn-sm btn-primary" style="float:right;" target="_blank">Read More </a>
        </div>
      </div>
    <?php } ?>
  </div>
</div>